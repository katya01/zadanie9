import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

import static org.testng.AssertJUnit.assertEquals;

public class TestLambdaDemo {
    @DataProvider
    public static Object[][] testCheckStringLambdaData(){
        return new Object[][]{
                {LambdaDemo.getStringLength, "Abc", 3},
                {LambdaDemo.getStringLength, "Vova", 4},
                {LambdaDemo.getStringLength, "", 0},
                {LambdaDemo.getStringLength, null, null},
                {LambdaDemo.getStringLength, "Painted", 7},
                {LambdaDemo.getFirstCharacterInString, "Abc", 'A'},
                {LambdaDemo.getFirstCharacterInString, "subscribe", 's'},
                {LambdaDemo.getFirstCharacterInString, "", null},
                {LambdaDemo.getFirstCharacterInString, null, null},
                {LambdaDemo.getFirstCharacterInString, "+Wolf", '+'},
                {LambdaDemo.getAmountsOfWordsInString, "Ab", 1},
                {LambdaDemo.getAmountsOfWordsInString, "", 0},
                {LambdaDemo.getAmountsOfWordsInString, null, 0},
                {LambdaDemo.getAmountsOfWordsInString, "Ab,Aw,Qqw,zxc", 4},
                {LambdaDemo.getAmountsOfWordsInString, "Ab.QQ|ds,Qwe!sdz*asd", 2}
        };
    }

    @Test (dataProvider = "testCheckStringLambdaData")
    public static void testCheckStringLambda(Function<String, ?> lambda, String string, Object res){
        assertEquals(lambda.apply(string), res);
        assertEquals(LambdaRunner.checkStringLambda(lambda, string), res);

    }

    @DataProvider
    public static Object[][] testCheckData(){
        return new Object[][]{
                {LambdaDemo.checkSpaceInString, "ASDsd", true},
                {LambdaDemo.checkSpaceInString, "AS qwZ", false},
                {LambdaDemo.checkSpaceInString, " cDsQ", false},
                {LambdaDemo.checkSpaceInString, "ARrgd ", false},
                {LambdaDemo.checkSpaceInString, " ", false},
                {LambdaDemo.checkSpaceInString, "", true},
                {LambdaDemo.checkSpaceInString, null, true},
        };
    }

    @Test(dataProvider = "testCheckData")
    public static void testCheck(Predicate<String> lambda, String string, boolean res){
        assertEquals(lambda.test(string), res);
        assertEquals(LambdaRunner.check(lambda, string), res);
    }

    @DataProvider
    public static Object[][] testCheckHumanLambdaData() throws Exception {
        return new Object[][]{
                {LambdaDemo.getAgeOfHuman, null, null},
                {LambdaDemo.getSurnameNamePatronymic, new Human("Petrov", "Petr", "Petrovich",
                        1990, Gender.MALE), "Petrov Petr Petrovich"},
                {LambdaDemo.getSurnameNamePatronymic, new Human("Vasilev", "Oleg", "Viktorovich",
                        1899, Gender.MALE), "Vasilev Oleg Viktorovich"},
                {LambdaDemo.getSurnameNamePatronymic, new Human("Petrova", "Hope", "Valerevna",
                        2001, Gender.FEMALE), "Petrova Hope Valerevna"},
                {LambdaDemo.getSurnameNamePatronymic, null, null},
                {LambdaDemo.makeOlder, new Human("Petrov", "Petr", "Petrovich",
                        1991, Gender.MALE), new Human("Petrov", "Petr", "Petrovich",
                        1990, Gender.MALE)},
                {LambdaDemo.makeOlder, new Human("Petrov", "Petr", "Petrovich",
                        800, Gender.MALE), new Human("Petrov", "Petr", "Petrovich",
                        799, Gender.MALE)},
                {LambdaDemo.makeOlder, null, null},
                {LambdaDemo.makeOlder, new Human("Petrova", "Faith", "Viktorovna",
                        1999, Gender.FEMALE), new Human("Petrova", "Faith", "Viktorovna",
                        1998, Gender.FEMALE)}
        };
    }

    @Test(dataProvider = "testCheckHumanLambdaData")
    public static <T extends Human> void testCheckHumanLambda(Function<T, ?> lambda, T human, Object res){
        assertEquals(lambda.apply(human), res);
        assertEquals(LambdaRunner.checkHumanLambda(lambda, human), res);
    }

    @DataProvider
    public static Object[][] testBiPredicateHumanData() throws Exception {
        return new Object[][]{
                {LambdaDemo.compareSurname, new Human("Petrov", "Petr", "Petrovich",
                        1979, Gender.MALE), new Human("Petrov", "Petr", "Petrovich",
                        1979, Gender.MALE), true},
                {LambdaDemo.compareSurname, new Human("Petrov", "Petr", "Petrovich",
                        1979, Gender.MALE), new Human("Petrova", "Hope", "Viktorovna",
                        2002, Gender.FEMALE), false},
                {LambdaDemo.compareSurname, new Human("Petrov", "Petr", "Petrovich",
                        1979, Gender.MALE), new Human("Petrov", "Eugenii", "Stepanovich",
                        1988, Gender.MALE), true},
                {LambdaDemo.compareSurname, null, new Human("Petrov", "Petr", "Petrovich",
                        1979, Gender.MALE), false},
                {LambdaDemo.compareSurname, new Human("Petrov", "Petr", "Petrovich",
                        1979, Gender.MALE), null, false}
        };
    }

    @Test(dataProvider = "testBiPredicateHumanData")
    public static void testBiPredicateHuman(BiPredicate<Human, Human> lambda, Human Jhonny, Human Willy, boolean res){
        assertEquals(lambda.test(Jhonny, Willy), res);
        assertEquals(LambdaRunner.biPredicateHuman(lambda, Jhonny, Willy), res);
    }

    @DataProvider
    public static Object[][] testСheckAgeCheckerData() throws Exception{
        return new Object[][]{
                {LambdaDemo.checkMaxAge,  new Human("Qwedsa", "Petr", "adasd",
                        1979, Gender.MALE), new Human("Petrasdaov", "ASDsad", "asd",
                        1999, Gender.MALE), new Human("asd", "Petr", "zxcas",
                        1939, Gender.MALE), 100, true},
                {LambdaDemo.checkMaxAge, null, new Human("Petrasdaov", "ASDsad", "asd",
                        1999, Gender.MALE), new Human("asd", "Petr", "zxcas",
                        1939, Gender.MALE), 100, false},
                {LambdaDemo.checkMaxAge,  new Human("Qwedsa", "Petr", "adasd",
                        1979, Gender.MALE), new Human("Petrasdaov", "ASDsad", "asd",
                        1999, Gender.MALE), new Human("asd", "Petr", "zxcas",
                        1939, Gender.MALE), 1, false},
                {LambdaDemo.checkMaxAge,  new Human("Qwedsa", "Petr", "adasd",
                        2010, Gender.MALE), new Human("Petrasdaov", "ASDsad", "asd",
                        2000, Gender.MALE), new Human("asd", "Petr", "zxcas",
                        2015, Gender.MALE), 7, false},
                {LambdaDemo.checkMaxAge,  new Human("Qwedsa", "Petr", "adasd",
                        2010, Gender.MALE), new Human("Petrasdaov", "ASDsad", "asd",
                        2000, Gender.MALE), new Human("asd", "Petr", "zxcas",
                        2015, Gender.MALE), 25, true},
                {LambdaDemo.checkMaxAge,  new Human("Qwedsa", "Petr", "adasd",
                        2010, Gender.MALE), new Human("Petrasdaov", "ASDsad", "asd",
                        2000, Gender.MALE), new Human("asd", "Petr", "zxcas",
                        2015, Gender.MALE), null, false}
        };
    }

    @Test(dataProvider = "testСheckAgeCheckerData")
    public static void testСheckAgeChecker(AgeChecker lambda, Human Jhon, Human Bill, Human Tim,
                                           Integer maxAge, boolean res){
        assertEquals(lambda.checkAge(Jhon, Bill, Tim, maxAge), res);
        assertEquals(LambdaRunner.checkAgeChecker(lambda, Jhon, Bill, Tim, maxAge), res);
    }
}