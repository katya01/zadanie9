import java.util.Objects;

public class Human {

    private String lastName;//фамилия
    private String firstName;//имя
    private String middleName;//отчество
    private int age;
    private Gender gender;

    public static final int CURRENT_YEAR = 2020;

    public Human(String lastName, String firstName, String middleName, int age, Gender gender){
        if(age<0 || age>CURRENT_YEAR){
            throw new IllegalArgumentException("Incorrect number of  age");
        }
        this.gender = gender;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.age = age;
    }

    public Human(Human copy){
        this.lastName = copy.lastName;
        this.firstName = copy.firstName;
        this.middleName = copy.middleName;
        this.age = copy.age;
        this.gender = copy.gender;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age)
    {
        if(age<0 || age>CURRENT_YEAR){
            throw new IllegalArgumentException("Incorrect number of  age");
        }
        this.age = age;
    }


    public Gender getGender(){
        return gender;
    }

    public void setGender(Gender gender){
        this.gender = gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age &&
                Objects.equals(lastName, human.lastName) &&
                Objects.equals(firstName, human.firstName) &&
                Objects.equals(middleName, human.middleName) &&
                gender == human.gender;
    }

    @Override
    public int hashCode() {

        return Objects.hash(lastName, firstName, middleName, age, gender);
    }
}